<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    <div class="site-container no-bg no-border">
        <!-- site title -->
        <div class="site-title-container">
            <div class="site-title">
                <p>Your new theme</p>
            </div>
        </div>
    </div>
    
    <div class="site-container">
        
        <div class="content-container container">
            <div class="menu-container">
                <?php 
                print theme('links',array('links'=>$main_menu));
            ?>
            </div>
            
            <div class="content clearfix">
                
                <h1><?php print $title; ?></h1>

            <?php if ($messages): ?>
                <!-- messages will go here -->
                <div id="messages">
                    <div class="section clearfix">
                  <?php print $messages; ?>
                </div></div> <!-- /.section, /#messages -->
            <?php endif; ?>
                
                <!-- tabs will go here -->
                <?php if ($tabs): ?>
                <div class="tab-container container">
                    <?php print render($tabs); ?>
                </div>
            <?php endif; ?>
                
                <!-- left column region -->
                <div class="left-column column region one-fourth left">
                    <div class="inner">

                        <?php if($page['right_callout']): ?>
                <div class="<?php print $variables['gambit_widthClasses']['column'] ?>">
                    <?php print render($page['right_callout']); ?>
                </div>
            <?php endif; ?>

                </div>

                </div>

                <div class="main-content three-fourths left">
                    
                    <div class="<?php print $variables['gambit_widthClasses']['main'] ?>">
                <?php
                    print render($page['content']);
                ?>
            
            <?php if($page['left_callout']): ?>
                <div class="<?php print $variables['gambit_widthClasses']['column'] ?>">
                    <?php print render($page['left_callout']); ?>
                </div>
            <?php endif; ?>

                 </div>
            </div>
        </div>
        
        <!-- footer region -->
        <div class="footer-container container">
            <div class="footer-content inner-container">
                <?php print render($page['footer']); ?>
            </div>
        </div>
        
    </div>
</body>

</html>
